var swiper = new Swiper('.modeling__slider', {
    slidesPerView: 1,
    slidesPerGroup: 1,
    // loop: true,
    // loopFillGroupWithBlank: true,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 20
        },
    }
});


/**
 * Download modal open and close
 */

$(document).ready(function(){
    // $('.modal-download').on('click', function(){
    //     $(this).hide()
    // })

    $('.download-close_button').on('click', function(){
        $('.modal-download').hide()
    })

    $('.download-button').on('click', function(){
        $('.modal-download').css('display', 'flex')
    })
})


/**
 * Form validation writeus
 */

$(document).ready(function(){

    const form = document.getElementById('download-form');
    const username = document.getElementById('download-form-input_name');
    const email = document.getElementById('download-form-input_email');
    const checkbox = document.getElementById('download-form-input_checkbox');
    const problem = document.getElementById('download-form-input_problem');

    form.addEventListener('submit', e => {
        e.preventDefault();

        checkInputs();
    });

    function checkInputs() {
        // trim to remove the whitespaces
        const usernameValue = username.value.trim();
        const emailValue = email.value.trim();

        const problemValue = problem.value.trim()

        if(usernameValue === '') {
            setErrorFor(username, 'Username cannot be blank');
        } else {
            setSuccessFor(username);
        }

        if(problemValue === '') {
            setErrorFor(problem, 'Username cannot be blank');
        } else {
            setSuccessFor(problem);
        }

        if(emailValue === '') {
            setErrorFor(email, 'Incorrect E-mail');
        } else if (!isEmail(emailValue)) {
            setErrorFor(email, 'Not a valid email');
        } else {
            setSuccessFor(email);
        }

        if(checkbox.checked === false){
            setErrorFor(checkbox, 'Please check');
            document.querySelector('.checkbox-warning').style.display = 'block'
        } else {
            setSuccessFor(checkbox);
            document.querySelector('.checkbox-warning').style.display = 'none'
        }
    }

    function setErrorFor(input, message) {
        const formControl = input.parentElement;
        const small = formControl.querySelector('small');
        formControl.classList.remove('success');
        formControl.classList.add('error')
        small.innerText = message;
    }

    function setSuccessFor(input) {
        const formControl = input.parentElement;
        formControl.classList.remove('error')
        formControl.classList.add('success');
    }

    function isEmail(email) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }

})


/**
 * Form validation download
 */

$(document).ready(function(){

    const form = document.getElementById('download-form-load');
    const username = document.getElementById('download-form-input_name-load');
    const email = document.getElementById('download-form-input_email-load');
    const checkbox = document.getElementById('download-form-input_checkbox-load');

    form.addEventListener('submit', e => {
        e.preventDefault();

        checkInputs();
    });

    function checkInputs() {
        // trim to remove the whitespaces
        const usernameValue = username.value.trim();
        const emailValue = email.value.trim();


        if(usernameValue === '') {
            setErrorFor(username, 'Username cannot be blank');
        } else {
            setSuccessFor(username);
        }

        if(emailValue === '') {
            setErrorFor(email, 'Incorrect E-mail');
        } else if (!isEmail(emailValue)) {
            setErrorFor(email, 'Not a valid email');
        } else {
            setSuccessFor(email);
        }

        if(checkbox.checked === false){
            setErrorFor(checkbox, 'Please check');
            document.querySelector('.checkbox-warning').style.display = 'block'
        } else {
            setSuccessFor(checkbox);
            document.querySelector('.checkbox-warning').style.display = 'none'
        }
    }

    function setErrorFor(input, message) {
        const formControl = input.parentElement;
        const small = formControl.querySelector('small');
        formControl.classList.remove('success');
        formControl.classList.add('error')
        small.innerText = message;
    }

    function setSuccessFor(input) {
        const formControl = input.parentElement;
        formControl.classList.remove('error')
        formControl.classList.add('success');
    }

    function isEmail(email) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }

})


/**
 * Input file text and remove handler
 */

$(document).ready(function(){

    $('#sm-ip-1').on('change',function(event){
        var name = event.target.files[0].name;
        $('#file-name').text(name).addClass('span-text-upload');
    })

    $('.span-text').on('click', function(){
        document.querySelector('#sm-ip-1').value = ''
        $('.span-text').removeClass('span-text-upload').text('');
    })


})

/**
 * Open menu classes
 */

$(document).ready(function(){
    if($(window).width() < 1024){
        $('.mst-header__item-arrow').each(function(){
            // $(this).parents('.mst-header__nav-item').find('.mst-header__submenu').removeClass('mst-header__submenu_active')
            $(this).on('click', function(){
                $(this).parents('.mst-header__nav-item').find('.mst-header__submenu').remove('mst-header__submenu_active')

                $(this).parents('.mst-header__nav-item').find('.mst-header__submenu').toggleClass('mst-header__submenu_active')
                $(this).parents('.mst-header__nav-item').find('.mst-header__item-arrow').toggleClass('mst-header__item-arrow_active')
            })
        })

        $(window).on('resize orientationchange', function(){
            $('.mst-header__nav').removeClass('ios-full-screen')
            $(this).parents('.mst-header__nav-item').find('.mst-header__submenu').removeClass('mst-header__submenu_active')

        })

        $('.mst-header__languages').on('click', function(){
            $('.mst-header__languages-list').toggleClass('mst-header__languages-list_active')
        })
    } else {

        return false
    }

})




/**
 * Toggle mobile menu
 */

$(document).ready(function(){
    var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    $('.mst-header__menu-icon').on('click', function(){
        $('.mst-header__nav').addClass('mst-header__nav_active')
        $('.mst-header__nav-list').addClass('mst-header__nav-list_active')
        $('.mst-header__action').addClass('mst-header__action_active')
        $('.mst-header__menu-icon').addClass('mst-header__menu_false')
        $('.mst-header__close-icon').addClass('mst-header__menu_true')
        if(iOS){
            // $('.mst-header__nav_active').css('height', '-webkit-fill-available')
            // $('.mst-header__nav_active').css('height', '96vh')
            $('.mst-header__nav').addClass('ios-full-screen')
        } else {
            $('.mst-header__nav').removeClass('ios-full-screen')
        }
    })

    $('.mst-header__close-icon').on('click', function(){
        $('.mst-header__nav').removeClass('mst-header__nav_active')
        $('.mst-header__nav-list').removeClass('mst-header__nav-list_active')
        $('.mst-header__action').removeClass('mst-header__action_active')
        $('.mst-header__menu-icon').removeClass('mst-header__menu_false')
        $('.mst-header__close-icon').removeClass('mst-header__menu_true')
        $('.mst-header__nav').removeClass('ios-full-screen')

    })

    $(window).on('resize orientationchange',function () {
        $('.mst-header__nav').removeClass('mst-header__nav_active')
        $('.mst-header__nav-list').removeClass('mst-header__nav-list_active')
        $('.mst-header__action').removeClass('mst-header__action_active')
        $('.mst-header__menu-icon').removeClass('mst-header__menu_false')
        $('.mst-header__close-icon').removeClass('mst-header__menu_true')
        $('.mst-header__submenu').removeClass('mst-header__submenu_active')
    })
})


$(document).ready(function(){
    // modal

    const searchButton = document.querySelector(".header-search__button");
    const searchModalWindow = document.querySelector(".modal-search");
    const closeModalButton = document.querySelector(".modal-search__close-button");

    searchButton.addEventListener("click", function () {
        searchModalWindow.classList.add("modal-search_active");
    });
    closeModalButton.addEventListener("click", function () {
        searchModalWindow.classList.remove("modal-search_active");
    });

// menu dropdown
// подразумевается, что на каждую иконку для открытия есть свой список, актуально только для хедера

    const openDropdown = document.querySelectorAll(".header-menu-list__item_drop");
    const openDropdownLink = document.querySelectorAll(
        ".header-menu-list__item_drop > a"
    );
    const dropdownList = document.querySelectorAll(".header-menu-list-drop");

    for (let i = 0; i < openDropdown.length; i++) {
        openDropdown[i].addEventListener("mousemove", function () {
            openDropdown[i].style.cssText = "color: blue";
            openDropdownLink[i].style.cssText = "color: blue";
            dropdownList[i].classList.add("header-menu-list-drop_active");
        });
        openDropdown[i].addEventListener("mouseout", function () {
            dropdownList[i].classList.remove("header-menu-list-drop_active");
            openDropdown[i].style.cssText = "";
            openDropdownLink[i].style.cssText = "";
        });
    }

// close coockie
    const coockieContainer = document.querySelector(".container_coockie");

    document.querySelector(".coockie-btn").addEventListener("click", function (e) {
        e.preventDefault();
        localStorage.setItem("confirm", 1);
        coockieContainer.classList.add("container_coockie_close");
    });

    window.onload = function () {
        if (!localStorage.getItem("confirm")) {
            coockieContainer.classList.remove("container_coockie_close");
        }
    };
})

$(document).ready(function () {
    var swiper = new Swiper('.main-page__slider', {
        slidesPerView: 1,
        spaceBetween: 30,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            560: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 30,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            },
        }
    });
})


/**
 * Modal Search
 */


const openModalButtons = document.querySelectorAll('[data-modal-target]')
const closeModalButtons = document.querySelectorAll('[data-close-button]')
const overlay = document.getElementById('overlaySearch');

openModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = document.querySelector(button.dataset.modalTarget)
        openModal(modal)
        $('.modalSearch__input').focus();
    })
})

closeModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.modalSearch'); //select parent element html
        closeModal(modal)
    })
})

overlay.addEventListener('click', () => {
    const modals = document.querySelectorAll('.modalSearch.active')
    modals.forEach(modal => {
        closeModal(modal)
    })
})

function openModal(modal){
    // if(modal == null){
    //     return
    // }
    if(modal == null) return
    modal.classList.add('active')
    overlay.classList.add('active')
}

function closeModal(modal){
    // if(modal == null){
    //     return
    // }
    if(modal == null) return
    modal.classList.remove('active')
    overlay.classList.remove('active')
}

// $(document).ready(function(){
//     $('.fenix3__left-sidebar-arrow, .fenix3__left-sedebar-link').each(function(){
//         $(this).on('click', function(){
//             // $('.fenix3__left-sidebar-submenu').removeClass('fenix3__left-sidebar-submenu_active')
//
//             $(this).parents('.fenix3__left-sedebar-item').find('.fenix3__left-sidebar-submenu').toggleClass('fenix3__left-sidebar-submenu_active')
//             $(this).parents('.fenix3__left-sedebar-item').find('.fenix3__left-sidebar-arrow').toggleClass('fenix3__left-sidebar-arrow_active')
//
//         })
//     })
// })


$(document).ready(function(){

    let a = []

    $('.fenix3__left-sedebar-item').each(function(n){
        $(this).attr('id', n)
        $(this).on('click', function(e){


            $(this)
                .find('.fenix3__left-sedebar-link-features')
                .toggleClass('fenix3__left-sedebar-link_active-inner')
            $(this)
                .find('.fenix3__left-sidebar-submenu')
                .toggleClass('fenix3__left-sidebar-submenu_active')
            $(this)
                .find('.fenix3__left-sidebar-arrow')
                .toggleClass('fenix3__left-sidebar-arrow_active')


            // if($('.fenix3__left-sedebar-list').innerHeight() > 900){
            //     $('.fenix3__left-sedebar-list').addClass('fenix3__left-sedebar-list_scroll')
            // }

            // console.log($('.fenix3__left-sedebar-list').innerHeight())


            // if($('.fenix3__left-sedebar-link_active-inner').length >= 2){
            //     if($(window).width() >= 1024){
            //         if($('.fenix3__left-sedebar-list').innerHeight() > 900){
            //             $('.fenix3__left-sedebar-list').css({
            //                 height: '800px',
            //                 overflowX: 'scroll',
            //                 overflowY: 'visible'
            //             })
            //         } else {
            //             $('.fenix3__left-sedebar-list').css({
            //                 height: 'auto',
            //                 overflowX: 'visible'
            //             })
            //         }
            //     }
            // }
            if($(window).width() >= 1024){
                if($('.fenix3__left-sedebar-link_active-inner').length > 0){
                    $('.fenix3__left-sedebar-list').addClass('fenix3__left-sedebar-list_scroll')
                } else {
                    $('.fenix3__left-sedebar-list').removeClass('fenix3__left-sedebar-list_scroll')
                }
            }

            if($(window).width() >= 1366){
                if($('.fenix3__left-sedebar-link_active-inner').length > 2){
                    $('.fenix3__left-sedebar-list').addClass('fenix3__left-sedebar-list_scroll')
                } else {
                    $('.fenix3__left-sedebar-list').removeClass('fenix3__left-sedebar-list_scroll')
                }
            }

        })
    })

})

/**
 * Highlight on scroll for main elements
 */


$(document).ready(function(){
    var contentSection = $('.fenix3__main-body-block');
    var navigation = $('.fenix3__left-sedebar-list');

    //when a nav link is clicked, smooth scroll to the section
    navigation.on('click', '.fenix3__left-sedebar-item a', function(event){
        event.preventDefault(); //prevents previous event
        smoothScroll($(this.hash));
    });

    //update navigation on scroll...
    $(window).on('scroll', function(){

        updateNavigation();
    })
    //...and when the page starts
    updateNavigation();

    /////FUNCTIONS
    function updateNavigation(){
        contentSection.each(function(){
            var sectionName = $(this).attr('id');
            var navigationMatch = $('.fenix3__left-sedebar-list a[href="#' + sectionName + '"]');

            if( $(this).position().top -100 <= $(window).scrollTop())
            {
                $('.fenix3__left-sedebar-link').removeClass('fenix3__left-sedebar-link_active-inner');

                navigationMatch.parents('.fenix3__left-sedebar-link').addClass('fenix3__left-sedebar-link_active-inner');
            }
            else {
                navigationMatch.parents('.fenix3__left-sedebar-link').removeClass('fenix3__left-sedebar-link_active-inner');
            }
        });
    }
    function smoothScroll(target){
        $('body,html').animate({
            // scrollTop: target.offset().top - 100
            scrollTop: ($(window).width() <= 1024) ? target.offset().top - 60 : target.offset().top - 90
        }, 200);

        // if(target[0].id === 'section1'){
        //     $('body,html').animate({
        //         // scrollTop: target.offset().top - 100
        //         scrollTop: ($(window).width() < 1024) ? target.offset().top - 60 : target.offset().top - 500
        //     }, 200);
        // }
    }
});


/**
 * Highlight on scroll for submenu elements
 */
$(document).ready(function(){
    var contentSection = $('.fenix3__main-body-block');
    var navigation = $('.fenix3__left-sidebar-submenu');

    //when a nav link is clicked, smooth scroll to the section
    navigation.on('click', '.fenix3__left-sidebar-submenu-item a', function(event){
        event.preventDefault(); //prevents previous event
        smoothScroll($(this.hash));
    });

    //update navigation on scroll...
    $(window).on('scroll', function(){
        updateNavigation();
    })
    //...and when the page starts
    updateNavigation();

    /////FUNCTIONS
    function updateNavigation(){
        contentSection.each(function(){
            var sectionName = $(this).attr('id');
            var navigationMatch = $('ul.fenix3__left-sidebar-submenu a[href="#' + sectionName + '"]');

            if(  $(this).position().top -100 <= $(window).scrollTop())
            {
                $('.fenix3__left-sidebar-submenu-item').removeClass('fenix3__left-sedebar-link_active');

                navigationMatch.parents('.fenix3__left-sidebar-submenu-item').addClass('fenix3__left-sedebar-link_active');
            }
            else {
                navigationMatch.parents('.fenix3__left-sidebar-submenu-item').removeClass('fenix3__left-sedebar-link_active');
            }
        });

    }
    function smoothScroll(target){
        $('body,html').animate({
            // scrollTop: target.offset().top - 100
            scrollTop: ($(window).width() <= 1024) ? target.offset().top - 50 : target.offset().top - 90
        }, 200);

        if(target[0].id === 'section1'){
            $('body,html').animate({
                // scrollTop: target.offset().top - 100
                scrollTop: ($(window).width() <= 1024) ? target.offset().top - 60 : target.offset().top - 90
            }, 200);

        }

        console.log(target[0].classList)
    }
});

$(document).ready(function(){
    $(window).on('scroll', function(){
        if(window.scrollY >= 30){
            $('.fenix3-new__main-screen, .main-screen__sub-nav').css('z-index', 100)
        } else {
            $('.fenix3-new__main-screen, .main-screen__sub-nav').css('z-index', 1)
        }
    })
})

$(document).ready(function(){
    if($('.fenix3__left-sedebar-item').length <=4){
        return false
    } else {
        if($(window).width() >= 1220){
            $(this).scroll(function (e) {
                if(window.scrollY + document.querySelector('footer').scrollHeight * 2.5  > document.body.scrollHeight){
                    $('.fenix3__left-sedebar-list')
                        .css(
                            {
                                // height: $('.fenix3__left-sedebar-link').length * 64,
                                // height: ($('.fenix3__left-sedebar-link').length == 4) ? '220px' : '272px',
                                // 'overflow-y': 'scroll'
                            }
                        )
                } else {
                    // $('.fenix3__left-sedebar-list')
                    //     .css(
                    //         {
                    //             height: 'auto',
                    //             'overflow-y': 'visible'
                    //         }
                    //     )
                }
            })
        }
    }



    $(window).on('load resize orientationchange', function(){

        if ($(window).height() <= 768) {
            $('.fenix3__left-sedebar-list')
                .css(
                    {
                        // height: $('.fenix3__left-sedebar-link').length * 64,
                        // height: '272px',
                        // 'overflow-y': 'scroll'
                    }
                )
        } else {
            // $('.fenix3__left-sedebar-list')
            //     .css(
            //         {
            //             height: 'auto',
            //             'overflow-y': 'visible'
            //         }
            //     )
        }
    })

})



$(document).ready(function(){
    $('.mst-header__desktop-lang, .mst-header__languages-list').on('mouseover', function(){
        $('.mst-header__languages-list').addClass('mst-header__languages-list_active')
        $('.mst-header__desktop-item').css('color', '#002EA6')
        $('.mst-header__desktop-item-icon').addClass('mst-header__item-arrow_hover')
    })

    $('.mst-header__desktop-lang, .mst-header__languages-list').on('mouseout', function(){
        $('.mst-header__languages-list').removeClass('mst-header__languages-list_active')
        $('.mst-header__desktop-item').css('color', '#484953')
        $('.mst-header__desktop-item-icon').removeClass('mst-header__item-arrow_hover')

    })
})

// close coockie
$(document).ready(function(){
    const coockieContainer = document.querySelector(".container_coockie");

    document.querySelector(".coockie-btn").addEventListener("click", function (e) {
        e.preventDefault();
        localStorage.setItem("confirm", 1);
        coockieContainer.classList.add("container_coockie_close");
    });

    if (!localStorage.getItem("confirm")) {
        coockieContainer.classList.remove("container_coockie_close");
    }

})

$(document).ready(function(){
    if ($(window).width()<1024){
        $('.mst-header__submenu').each(function(){
            $(this).on('click', function(){
                // $(this).parents('.mst-header__item-link').find('.mst-header__link_wrapper').toggleClass('mst-header__link_wrapper_active')
                // $(this).parents('.mst-header__item-link').find('.mst-header__item-arrow').addClass('mst-header__item-arrow_hover')
            })

            // $(this).on('mouseout', function(){
            //     $(this).parents('.mst-header__item-link').find('.mst-header__link_wrapper').removeClass('mst-header__link_wrapper_active')
            //     // $(this).parents('.mst-header__item-link').find('.mst-header__item-arrow').removeClass('mst-header__item-arrow_hover')
            // })
        })
    }

})

/**
 * Change input label on focus
 */

$(document).ready(function(){
    $('.form-input').each(function(){
        $(this).on('focus', function(){
            $(this).parents('.writeus-input').find('.form-label').addClass('form-label_active')
            $(this).attr('placeholder', '')
        })

        $(this).on('focusout', function(){
            $(this).parents('.writeus-input').find('.form-label').removeClass('form-label_active')
            $(this).attr('placeholder', $(this).parents('.writeus-input').find('.form-label').text())
        })
    })

    $('.form-input').each(function(){
        $(this).on('keyup', function(){
            if($(this).val() === ''){
                $(this).parents('.writeus-input').find('.form-label').removeClass('form-label_active-fill')
            } else {
                $(this).parents('.writeus-input').find('.form-label').addClass('form-label_active-fill')
            }
        })

    })
})

$(document).ready(function(){
    const btns = document.querySelectorAll(".tabs__button");
    const tabContent = document.querySelectorAll(".tab-content");

    for (let i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", () => {
            addClassFunc(btns[i], "tabs__button--active");
            clearClassFunc(i, btns, "tabs__button--active");

            addClassFunc(tabContent[i], "tab-content--active");
            clearClassFunc(i, tabContent, "tab-content--active");
        });
    }

    function addClassFunc(elem, elemClass) {
        elem.classList.add(elemClass);
    }

    function clearClassFunc(indx, elems, elemClass) {
        for (let i = 0; i < elems.length; i++) {
            if (i === indx) {
                continue;
            }
            elems[i].classList.remove(elemClass);
        }
    }

})


/**
 * Buy page slider
 */

$(document).ready(function(){
    var fenix3BuyDescSlider = new Swiper('.fenix3-buy-desc__slider', {
        // effect: 'fade',
        pagination: {
            el: '.swiper-pagination',
        },
        breakpoints:{
            1024: {
                loop: false,
                noSwiping: true,
                slidesPerView: 2,
                centeredSlides: false,
                spaceBetween: 16,
                allowSlidePrev: false,
                allowSlideNext: false
            },
            1366: {
                spaceBetween: 32,
                loop: false,
                noSwiping: true,
                slidesPerView: 2,
                centeredSlides: false,
                allowSlidePrev: false,
                allowSlideNext: false
            }
        }
    });
})

/**
 * Faq tabs
 */

$(document).ready(function(){
    const accordionItemHeaders = document.querySelectorAll(".accordion-item-header");

    accordionItemHeaders.forEach(accordionItemHeader => {
        accordionItemHeader.addEventListener("click", event => {

            // Uncomment in case you only want to allow for the display of only one collapsed item at a time!

            // const currentlyActiveAccordionItemHeader = document.querySelector(".accordion-item-header.active");
            // if(currentlyActiveAccordionItemHeader && currentlyActiveAccordionItemHeader!==accordionItemHeader) {
            //   currentlyActiveAccordionItemHeader.classList.toggle("active");
            //   currentlyActiveAccordionItemHeader.nextElementSibling.style.maxHeight = 0;
            // }

            accordionItemHeader.classList.toggle("active");
            const accordionItemBody = accordionItemHeader.nextElementSibling;
            if(accordionItemHeader.classList.contains("active")) {
                accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
            }
            else {
                accordionItemBody.style.maxHeight = 0;
            }

        });
    });
})


/**
 * Слайдер на странице Support / Поддержка
 */
$(document).ready(function(){
    var swiper = new Swiper('.fenix3-support-video__slider', {
        spaceBetween: 15,
        pagination: {
            el: '.swiper-pagination',
        },
        breakpoints: {
            425: {
                slidesPerView: 2,
                spaceBetween: 16
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 32,
                loop: false,
                noSwiping: true,
                centeredSlides: false,
                allowSlidePrev: false,
                allowSlideNext: false,
                pagination: false
            },
        }
    })
})

/**
 * Скрытие / Открытие списка фильтров
 */

$(document).ready(function(){
    $('.fenix3-res__filter-item-block').on('click', function(){
        $('.fenix3-res__filter-inner').toggleClass('fenix3-res__filter-inner_active')
        $('.fenix3-res__filter-item-arrow').toggleClass('fenix3-res__filter-item-arrow_active')
    })
})

/**
 * Фильтр на странице результатов
 */

$(document).ready(function(){
    $('.fenix3-res__filter-inner-item a').each(function(){
        $(this).on('click', function(e){
            e.preventDefault()
            const filter = $(this)
            $('.fenix3-res__block-filter').each(function(){
                if($(this).data('filter') === filter.data('filter')){
                    $(this).parents('.fenix3-res__block').css('display', 'block')
                } else {
                    $(this).parents('.fenix3-res__block').css('display', 'none')
                }
            })
        })
    })
})


/**
 * Изменение размера text area в зависимости от размера контента
 */

$(document).ready(function(){
    var textarea = document.querySelector('textarea');

    textarea.addEventListener('keydown', autosize);

    function autosize(){
        var el = this;
        setTimeout(function(){
            el.style.cssText = 'height:auto; padding:0 0 5px 0';
            // for box-sizing other than "content-box" use:
            // el.style.cssText = '-moz-box-sizing:content-box';
            el.style.cssText = 'height:' + el.scrollHeight + 'px';
        },0);
    }
})

/**
 * Саб меню
 */

$(document).ready(function(){
    $('.main-screen__sublogo').on('click', function(){
        $(this).find('.main-screen__sublogo-arrow').toggleClass('main-screen__sublogo-arrow_active')
        $(this).parents('.main-screen__block').find('.main-screen__nav-wrapper').toggleClass('main-screen__nav-wrapper_active')
    })
})